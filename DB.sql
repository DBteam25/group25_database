drop database if exists db_25;
create database db_25;
use db_25;

drop table if exists types;
create table types (
   type_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
   type_name ENUM('S', 'B') NOT NULL
);

drop table if exists instruments;
create table instruments (
   instrument_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
   instrument_name VARCHAR(15) NOT NULL
);

insert into instruments (instrument_name) values ("Astronomica"), 
												 ("Borealis"), 
                                                 ("Celestial"), 
                                                 ("Deuteronic"),
                                                 ("Eclipse"),
                                                 ("Floral"),
                                                 ("Galactia"),
                                                 ("Heliosphere"),
                                                 ("Interstella"),
                                                 ("Jupiter"),
                                                 ("Koronis"),
                                                 ("Lunatic");


drop table if exists counter_parties;
create table counter_parties (
   counter_party_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
   counter_party_name VARCHAR(10) NOT NULL UNIQUE
);

insert into counter_parties (counter_party_name) values ("Lewis"),
														("Selvyn"),
                                                        ("Richard"),
                                                        ("Lina"),
                                                        ("John"),
                                                        ("Nidia");

select * from counter_parties;

drop table if exists deals;
create table deals (
   deal_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
   instrument_id INT UNSIGNED NOT NULL,
   counter_party_id INT UNSIGNED NOT NULL,
   price DECIMAL(16,11) UNSIGNED NOT NULL,
   type_id INT UNSIGNED NOT NULL,
   quantity INT UNSIGNED NOT NULL,
   time DATETIME NOT NULL,
   FOREIGN KEY (instrument_id) REFERENCES instruments(instrument_id),
   FOREIGN KEY (counter_party_id) REFERENCES counter_parties(counter_party_id),
   FOREIGN KEY (type_id) REFERENCES types(type_id)
);
select * from deals;


drop table if exists roles;
create table roles (
   role_id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
   role_name VARCHAR(20) NOT NULL
);

drop table if exists user_accounts;
create table user_accounts (
   user_account_id  INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
   name VARCHAR(20) NOT NULL,
   surname VARCHAR(20) NOT NULL,
   email VARCHAR(30) NOT NULL,
   role_id INT UNSIGNED NOT NULL,
   salt VARCHAR(30) NOT NULL,
   password_hashed VARCHAR(30) NOT NULL,
   FOREIGN KEY (role_id) REFERENCES roles(role_id)
);

insert into types (type_name) values ('S'), 
									 ('B');
/*
SELECT l.instrument_name, AVG(r.price) 
FROM instruments l
LEFT JOIN deals r using(instrument_id)
LEFT JOIN types t using(type_id)
WHERE t.type_name = 'B'
GROUP BY l.instrument_name;
*/
